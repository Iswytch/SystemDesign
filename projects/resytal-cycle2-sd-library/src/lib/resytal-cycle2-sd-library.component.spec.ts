import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResytalCycle2SdLibraryComponent } from './resytal-cycle2-sd-library.component';

describe('ResytalCycle2SdLibraryComponent', () => {
  let component: ResytalCycle2SdLibraryComponent;
  let fixture: ComponentFixture<ResytalCycle2SdLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResytalCycle2SdLibraryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResytalCycle2SdLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
