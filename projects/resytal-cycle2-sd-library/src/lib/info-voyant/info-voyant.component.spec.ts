import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoVoyantComponent } from './info-voyant.component';

describe('InfoVoyantComponent', () => {
  let component: InfoVoyantComponent;
  let fixture: ComponentFixture<InfoVoyantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoVoyantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoVoyantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
