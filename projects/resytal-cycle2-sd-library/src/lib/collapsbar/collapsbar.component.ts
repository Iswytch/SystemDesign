import { Component, OnInit } from '@angular/core';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-collapsbar',
  templateUrl: './collapsbar.component.html',
  styleUrls: ['./collapsbar.component.css'],
  animations: [
    trigger('fiche-synthese', [
      state('enabled', style({
        height: '15vw',
        'padding-top': '2vw'
      })),
      state('disabled', style({
        height: '0vw'
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.09,.81,.61,1)')
      ]),
    ]),
    trigger('img', [
      state('enabled', style({
        transform: 'rotate(180deg)',
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.09,.81,.61,1)')
      ]),
    ]),
    trigger('btn-all-collapsbar', [
      transition('* => animate', [
        animate('0.35s', keyframes([
          style({'background-color': '#D2D2D2'}),
          style({scale: '0.87'}),
          style({scale: '1', 'background-color': '#707070', color: '#FFFFFF'})
        ]))
      ])
    ])
  ],
})
export class CollapsbarComponent implements OnInit {

  public activate1 = false;
  public activate11 = false;

  public isClickedDown = false;
  public isClickedUp = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation1(): void {
    this.activate1 = !this.activate1;
  }

  runAnimation11(): void {
    this.activate11 = !this.activate11;
  }


  // Permet de pouvoir recliquer sur le bouton.
  // La durée du timeout doit être supérieur ou égal à la durée de l'animation.
  runAnimationDown(): void {
    this.isClickedDown = true;
    setTimeout(() => {
      this.isClickedDown = false;
    }, 400);
    this.activate1 = true;
    this.activate11 = true;
  }

  runAnimationUp(): void {
    this.isClickedUp = true;
    setTimeout(() => {
      this.isClickedUp = false;
    }, 400);
    this.activate1 = false;
    this.activate11 = false;
  }

}
