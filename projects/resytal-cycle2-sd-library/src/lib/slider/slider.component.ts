import { Component, OnInit } from '@angular/core';
import { CdkDragEnd, CdkDragMove } from '@angular/cdk/drag-drop';

@Component({
  selector: 'lib-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  public actif=1;

  constructor() { }

  ngOnInit(): void {

  }

  /** Evenement quand bouge le bouton. */
  onDragMoved(event: CdkDragMove) {

     // Position de la souris
     const mousePosition = {
      x: event.pointerPosition.x,
      y: event.pointerPosition.y
    };

    var coordXSlider = document.getElementById('container2')!.getBoundingClientRect().left;
    var coordYSlider = document.getElementById('container2')!.getBoundingClientRect().top;

    console.log(mousePosition);
    console.log("Y "+coordYSlider+"   X "+coordXSlider);

    if (mousePosition.y+35 > coordYSlider && mousePosition.y < coordYSlider+35 &&
      mousePosition.x > coordXSlider-6 && mousePosition.x < coordXSlider+20)
    {
      this.actif=1;
      document.getElementById('slider')!.style.left='0px';
    }
    if (mousePosition.y+35 > coordYSlider && mousePosition.y < coordYSlider+35 &&
      mousePosition.x > coordXSlider+110 && mousePosition.x < coordXSlider+180)
    {
      this.actif=2;
      document.getElementById('slider')!.style.left='145px';
    }
    if (mousePosition.y+35 > coordYSlider && mousePosition.y < coordYSlider+35 &&
      mousePosition.x > coordXSlider+250 && mousePosition.x < coordXSlider+300)
    {
      this.actif=3;
      document.getElementById('slider')!.style.left='297px';
    }

  }

    /** Evenement quand le drag and drop est terminé. */
  onDragEnded(event: CdkDragEnd) {
    switch (this.actif) {

      //Angle le plus haut
      case 1:
        document.getElementById('mask')!.style.transform = 'translate(-9.6px,-57.5px)';
        break;
      case 2:
        document.getElementById('mask')!.style.transform = 'translate(140px,-57.5px)';
        break;
      case 3:
        document.getElementById('mask')!.style.transform = 'translate(288px,-57.5px)';
        break;

      default:
        break;
    }
  }

}
