import { Component, OnInit } from '@angular/core';
import { animate, keyframes, transition, trigger, style } from '@angular/animations';

@Component({
  selector: 'lib-btn-action',
  templateUrl: './btn-action.component.html',
  styleUrls: ['./btn-action.component.css'],
  animations: [
    trigger('btn-action-animation', [
      transition('* => animate', [
        animate('0.35s', keyframes([
          style({'background-color': '#D2D2D2'}),
          style({'scale': '0.87'}),
          style({'scale': '1', 'background-color': '#707070', 'color':'#FFFFFF'})
        ]))
      ])
    ])
  ]
})

export class BtnActionComponent implements OnInit {

  public isClicked = false;
  public exampleNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  // Permet de pouvoir recliquer sur le bouton.
  // La durée du timeout doit être supérieur ou égal à la durée de l'animation.
  runAnimation(): void {
    this.isClicked=true;
    setTimeout(() => {
      this.isClicked=false;
    }, 400);
  }

  example() {
    this.exampleNumber = this.exampleNumber+1;
  }

}
