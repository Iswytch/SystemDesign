import { Component, OnInit , NgZone } from '@angular/core';
import { AnimationItem } from 'lottie-web';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-btn-guide',
  templateUrl: './btn-guide.component.html',
  styleUrls: ['./btn-guide.component.css']

})
export class BtnGuideComponent implements OnInit {

  options: AnimationOptions = {
    path: '/assets/animation/animation_guide.json',
  };

  private animationItem!: AnimationItem;

  constructor(private ngZone: NgZone) { }

  ngOnInit(): void {
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    animationItem.autoplay=false;
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationItem.stop();
    });
  }

  play(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationItem.play();
    });
  }


}
