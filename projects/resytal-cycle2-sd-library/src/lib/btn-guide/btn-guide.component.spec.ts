import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnGuideComponent } from './btn-guide.component';

describe('BtnGuideComponent', () => {
  let component: BtnGuideComponent;
  let fixture: ComponentFixture<BtnGuideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnGuideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
