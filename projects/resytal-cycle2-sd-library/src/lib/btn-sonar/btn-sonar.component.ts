import { animate, keyframes, transition, trigger, style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-sonar',
  templateUrl: './btn-sonar.component.html',
  styleUrls: ['./btn-sonar.component.css'],
  animations: [
    trigger('sonarAnimation', [
      transition('* => animate', [
        animate('0.55s', keyframes([
          style({'scale': '1', 'background-color': '#D2D2D2', 'color':'#474747'}),
          style({'scale': '0.60'}),
          style({'scale': '1', 'background-color': '#707070', 'color':'#FFFFFF'})
        ]))
      ])
    ]),
    trigger('cercleAnimation', [
      transition('* => animate', [
        animate('0.55s', keyframes([
          style({'scale': '0.6'}),
          style({'scale': '1'}),
          style({'scale': '0.6'})
        ]))
      ])
    ])
  ]
})
  
export class BtnSonarComponent implements OnInit {

  public isClicked = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation(): void {
    this.isClicked=true;
    setTimeout(() => {
      this.isClicked=false;
    }, 550);
  } 

}
