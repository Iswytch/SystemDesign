import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
  animations: [
    trigger('mask', [
      state('enabled', style({
        transform: 'translate(1.042vw)'
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('0.5s cubic-bezier(.07,.42,.73,.48)')
      ]),
    ])
  ]
})
export class CheckboxComponent implements OnInit {

  public activateAnimMask = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimationMask(): void {
    this.activateAnimMask = !this.activateAnimMask;
  }

}
