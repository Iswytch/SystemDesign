import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnResetComponent } from './btn-reset.component';

describe('BtnResetComponent', () => {
  let component: BtnResetComponent;
  let fixture: ComponentFixture<BtnResetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnResetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
