import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-btn-selection-statut',
  templateUrl: './btn-selection-statut.component.html',
  styleUrls: ['./btn-selection-statut.component.css'],
  animations: [
    // Nom du déclencheur de l'animation
    trigger('btn-selection', [
      state('disabled', style({
        transform: 'translate(0.833vw,-0.833vw)',
        background: '#959595',
        'box-shadow' : 'inset 0px 3px 6px #000000'
      })),
      state('enabled', style({
        transform: 'translate(15px,-15px)',
        background: 'radial-gradient(closest-side at 50% 50%, #18FB53 0%, #0C7E2A 100%)'
      })),
    ]),
    trigger('border', [
      state('disabled', style({
        width: '2.6vw',
        height: '2.6vw'
      })),
      state('enabled', style({
        width: '2.4vw',
        height: '2.4vw'
      })),
    ]),
    trigger('text', [
      state('disabled', style({
        color: '#595959'
      })),
      state('enabled', style({
        color: 'black',
        'font-size': '0.75vw'
      })),
    ]),
  ],

})
export class BtnSelectionStatutComponent implements OnInit {

  public activate = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation(): void {
    this.activate = !this.activate;
  }

}
