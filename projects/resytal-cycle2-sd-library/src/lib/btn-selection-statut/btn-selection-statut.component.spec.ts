import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnSelectionStatutComponent } from './btn-selection-statut.component';

describe('BtnSelectionStatutComponent', () => {
  let component: BtnSelectionStatutComponent;
  let fixture: ComponentFixture<BtnSelectionStatutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnSelectionStatutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnSelectionStatutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
