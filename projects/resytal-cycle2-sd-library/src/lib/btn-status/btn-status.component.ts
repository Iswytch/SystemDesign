import { Component, OnInit } from '@angular/core';
import { CdkDragEnd, CdkDragMove } from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-btn-status',
  templateUrl: './btn-status.component.html',
  styleUrls: ['./btn-status.component.css']
})
export class BtnStatusComponent implements OnInit {

  public angle: number = 0;


  constructor() { }

  ngOnInit(): void {

  }

  /** Evenement quand bouge le bouton. */
  onDragMoved(event: CdkDragMove) {

    // Position de la souris
    const mousePosition = {
      x: event.pointerPosition.x,
      y: event.pointerPosition.y
    };

    var coordXBtn = document.getElementById('btn-status')!.getBoundingClientRect().left;
    var coordYBtn = document.getElementById('btn-status')!.getBoundingClientRect().top;


    if (mousePosition.y> coordYBtn+55 && mousePosition.y < coordYBtn+55+30 &&
        mousePosition.x > coordXBtn+85 && mousePosition.x < coordXBtn+85+40)
      {
      document.getElementById('btn-status')!.style.transform = 'rotate(' + 90 + 'deg)';
      this.angle = -1;
      console.log('angle -1');
    }
    if (mousePosition.y > coordYBtn+55 && mousePosition.y < coordYBtn+55+30 &&
        mousePosition.x > coordXBtn+45 && mousePosition.x < coordXBtn+45+30)
      {
      document.getElementById('btn-status')!.style.transform = 'rotate(' + 0 + 'deg)';
      this.angle = 0;
      console.log('angle 0');
    }
    if (mousePosition.y > coordYBtn+90 && mousePosition.y < coordYBtn+100+30 &&
        mousePosition.x > coordXBtn+45 && mousePosition.x < coordXBtn+45+30)
      {
      document.getElementById('btn-status')!.style.transform = 'rotate(' + -90 + 'deg)';
      this.angle = 1;
      console.log('angle 1');
    }
  }

  /** Evenement quand le drag and drop est terminé.
  Si le style du mask-pointer ne contient que top et left et il n'y a plus le transform, le mask ne se place pas au bon endroit (?ajouter un rotate de 0 deg?) */

  onDragEnded(event: CdkDragEnd) {
    switch (this.angle) {

      //Angle le plus haut
      case -1:
	      document.getElementById('mask-pointer')!.style.top = '27px';
        document.getElementById('mask-pointer')!.style.left = '70px';
        document.getElementById('mask-pointer')!.style.transform = 'rotate(' + 45 + 'deg)';

        break;

      //Angle du milieu
      case 0:
	      document.getElementById('mask-pointer')!.style.top = '28px';
        document.getElementById('mask-pointer')!.style.left = '32px';
        document.getElementById('mask-pointer')!.style.transform = 'rotate(' + -40 + 'deg)';

        console.log("case 0");
        break;

      //Angle le plus bas
      case 1:
      	document.getElementById('mask-pointer')!.style.top = '65px';
        document.getElementById('mask-pointer')!.style.left = '33px';
        document.getElementById('mask-pointer')!.style.transform = 'rotate(' + -140 + 'deg)';

        console.log("case 1");
        break;

      default:
        break;
    }
  }

}
