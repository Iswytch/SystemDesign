import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchMediumComponent } from './switch-medium.component';

describe('SwitchMediumComponent', () => {
  let component: SwitchMediumComponent;
  let fixture: ComponentFixture<SwitchMediumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchMediumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
