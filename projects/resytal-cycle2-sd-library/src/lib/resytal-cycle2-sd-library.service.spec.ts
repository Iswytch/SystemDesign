import { TestBed } from '@angular/core/testing';

import { ResytalCycle2SdLibraryService } from './resytal-cycle2-sd-library.service';

describe('ResytalCycle2SdLibraryService', () => {
  let service: ResytalCycle2SdLibraryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResytalCycle2SdLibraryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
