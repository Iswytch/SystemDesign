import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-switch-large',
  templateUrl: './switch-large.component.html',
  styleUrls: ['./switch-large.component.css'],
  animations: [
    trigger('switch-large', [
      state('enabled', style({
        transform: 'translate(155%)',
        background: 'linear-gradient(180deg, #E9E9E9 0%, #474747 100%)'
      })),
      state('disabled', style({
        background: 'linear-gradient(180deg, #9E9E9E 0%, #101010 100%)'
      })),
      transition('enabled <=> disabled', [
        animate('0.2s')
      ]),
    ]),
  ],
})
export class SwitchLargeComponent implements OnInit {

  public activate = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation(): void {
    this.activate = !this.activate;
  }

}
