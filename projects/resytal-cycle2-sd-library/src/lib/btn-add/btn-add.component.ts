import { Component, OnInit } from '@angular/core';
import { animate, keyframes, transition, trigger, style } from '@angular/animations';

@Component({
  selector: 'lib-btn-add',
  templateUrl: './btn-add.component.html',
  styleUrls: ['./btn-add.component.css'],
  animations: [
    trigger('plus-animation', [
      transition('* => animate', [
        animate('0.25s', keyframes([
          style({'transform': 'rotate(90deg)'})
        ]))
      ])
    ]),
  ]
})
export class BtnAddComponent implements OnInit {

  public blue : boolean = true;
  public isClicked = false;

  constructor() { }

  ngOnInit(): void {
  }

  // Permet de pouvoir recliquer sur le bouton.
  // La durée du timeout doit être supérieur ou égal à la durée de l'animation.
  runAnimation(): void {
    this.isClicked=true;
    setTimeout(() => {
      this.isClicked=false;
    }, 300);
  }

}
