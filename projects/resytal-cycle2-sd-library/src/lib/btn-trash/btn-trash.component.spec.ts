import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnTrashComponent } from './btn-trash.component';

describe('BtnTrashComponent', () => {
  let component: BtnTrashComponent;
  let fixture: ComponentFixture<BtnTrashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnTrashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnTrashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
