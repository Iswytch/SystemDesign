import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
@Component({
  selector: 'app-switch-medium',
  templateUrl: './switch-medium.component.html',
  styleUrls: ['./switch-medium.component.css'],
  animations: [
    // Nom du déclencheur de l'animation
    trigger('switch-medium', [
      //Etat enabled, déplace de la droite de 155% de la taille du bouton et change la couleur de fond
      state('enabled', style({
        transform: 'translate(155%)',
        background: 'linear-gradient(180deg, #E9E9E9 0%, #474747 100%)'
      })),
      //Etat "enabled", laisse ou remplace la couleur de fond (dépend de l'état précedent)
      state('disabled', style({
        background: 'linear-gradient(180deg, #9E9E9E 0%, #101010 100%)'
      })),
      //Transition entre l'état enabled et disabled dure 0.2 secondes
      transition('enabled <=> disabled', [
        animate('0.2s')
      ]),
    ]),
  ],
})
export class SwitchMediumComponent implements OnInit {

  public activate = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation(): void {
    this.activate = !this.activate;
  }
}
