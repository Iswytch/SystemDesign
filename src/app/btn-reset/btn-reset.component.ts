import { Component, OnInit } from '@angular/core';
import { animate, keyframes, transition, trigger, style } from '@angular/animations';

@Component({
  selector: 'app-btn-reset',
  templateUrl: './btn-reset.component.html',
  styleUrls: ['./btn-reset.component.css'],
  animations: [
    trigger('btn-reset-animation', [
      transition('* => btn-animate', [
        animate('0.35s', keyframes([
          style({'background-color': '#707070'}),
          style({'scale': '0.80'}),
          style({'scale': '1', 'background-color': '#474747'})
        ]))
      ])
    ]),
    trigger('btn-reset-arrow-animation', [
      transition('* => arrow-animate', [
        animate('0.5s', keyframes([
          style({}),
          style({'transform': 'rotate(360deg)'}),
        ]))
      ])
    ])
  ]
})
export class BtnResetComponent implements OnInit {

  public isClickedForBtn = false;
  public isClickedForArrow = false;

  constructor() { }

  ngOnInit(): void {
  }

  // Permet de pouvoir recliquer sur le bouton.
  // La durée du timeout doit être supérieur ou égal à la durée de l'animation.
  runAnimationBtn(): void {
    this.isClickedForBtn=true;
    setTimeout(() => {
      this.isClickedForBtn=false;
    }, 500);
  }

  runAnimationArrow(): void {
    this.isClickedForArrow=true;
    setTimeout(() => {
      this.isClickedForArrow=false;
    }, 500);
  }

}
