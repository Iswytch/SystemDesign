import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  dataNiv1 = [
    { nom: 'exemple 1' },
    { nom: 'exemple 2' },
    { nom: 'exemple 3' }
  ];

  dataNiv2 = [
    { nom: 'exemple 1.1' },
    { nom: 'exemple 1.2' },
    { nom: 'exemple 1.3' }
  ];


  constructor() { }

  ngOnInit(): void {
  }

}
