import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BtnSonarComponent } from './btn-sonar/btn-sonar.component';
import { InfoVoyantComponent } from './info-voyant/info-voyant.component';
import { SwitchLargeComponent } from './switch-large/switch-large.component';
import { SwitchMediumComponent } from './switch-medium/switch-medium.component';
import { BtnResetComponent } from './btn-reset/btn-reset.component';
import { BtnAddComponent } from './btn-add/btn-add.component';
import { BtnTrashComponent } from './btn-trash/btn-trash.component';
import { BtnStatusComponent } from './btn-status/btn-status.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { FicheSyntheseComponent } from './fiche-synthese/fiche-synthese.component';
import { BtnRadioComponent } from './btn-radio/btn-radio.component';
import { CollapsbarComponent } from './collapsbar/collapsbar.component';
import { MenuComponent } from './menu/menu.component';
import { BtnSelectionStatutComponent } from './btn-selection-statut/btn-selection-statut.component';
import { DragAndDropComponent } from './drag-and-drop/drag-and-drop.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { BtnServiceComponent } from './btn-service/btn-service.component';
import { SliderComponent } from './slider/slider.component';
import { BtnGuideComponent } from './btn-guide/btn-guide.component';
import { PreloaderComponent } from './preloader/preloader.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { FooterComponent } from './footer/footer.component';
import { BtnActionComponent } from './btn-action/btn-action.component';

// Note we need a separate function as it's required
// by the AOT compiler.
export function playerFactory() {
  return player;
}

@NgModule({
  declarations: [
    AppComponent,
    BtnSonarComponent,
    InfoVoyantComponent,
    SwitchLargeComponent,
    SwitchMediumComponent,
    BtnResetComponent,
    BtnAddComponent,
    BtnTrashComponent,
    BtnStatusComponent,
    FilterPanelComponent,
    FicheSyntheseComponent,
    BtnRadioComponent,
    CollapsbarComponent,
    MenuComponent,
    BtnSelectionStatutComponent,
    DragAndDropComponent,
    CheckboxComponent,
    BtnServiceComponent,
    SliderComponent,
    BtnGuideComponent,
    PreloaderComponent,
    DropdownComponent,
    FooterComponent,
    BtnActionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    DragDropModule,
    LottieModule.forRoot({ player: playerFactory })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
