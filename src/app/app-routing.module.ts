import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BtnSonarComponent } from './btn-sonar/btn-sonar.component';
import { InfoVoyantComponent } from './info-voyant/info-voyant.component';
import { SwitchLargeComponent } from './switch-large/switch-large.component';
import { SwitchMediumComponent } from './switch-medium/switch-medium.component';
import { BtnResetComponent } from './btn-reset/btn-reset.component';
import { BtnAddComponent } from './btn-add/btn-add.component';
import { BtnTrashComponent } from './btn-trash/btn-trash.component';
import { BtnStatusComponent } from './btn-status/btn-status.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { FicheSyntheseComponent } from './fiche-synthese/fiche-synthese.component';
import { BtnRadioComponent } from './btn-radio/btn-radio.component';
import { CollapsbarComponent } from './collapsbar/collapsbar.component';
import { MenuComponent } from './menu/menu.component';
import { BtnSelectionStatutComponent } from './btn-selection-statut/btn-selection-statut.component';
import { DragAndDropComponent } from './drag-and-drop/drag-and-drop.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { BtnServiceComponent } from './btn-service/btn-service.component';
import { SliderComponent } from './slider/slider.component';
import { BtnGuideComponent } from './btn-guide/btn-guide.component';
import { PreloaderComponent } from './preloader/preloader.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { FooterComponent } from './footer/footer.component';
import { BtnActionComponent } from './btn-action/btn-action.component';

const routes: Routes = [
  { path: 'btn-sonar', component: BtnSonarComponent },
  { path: 'voyant', component: InfoVoyantComponent },
  { path: 'switch-large', component: SwitchLargeComponent },
  { path: 'switch-medium', component: SwitchMediumComponent },
  { path: 'btn-reset', component: BtnResetComponent },
  { path: 'btn-add', component: BtnAddComponent },
  { path: 'btn-trash', component: BtnTrashComponent },
  { path: 'btn-status', component: BtnStatusComponent },
  { path: 'filter-panel', component: FilterPanelComponent },
  { path: 'fiche-synthese', component: FicheSyntheseComponent },
  { path: 'btn-radio', component: BtnRadioComponent },
  { path: 'collapsbar', component: CollapsbarComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'btn-selection-statut', component: BtnSelectionStatutComponent },
  { path: 'drag-and-drop', component: DragAndDropComponent },
  { path: 'btn-service', component: BtnServiceComponent },
  { path: 'checkbox', component: CheckboxComponent },
  { path: 'slider', component: SliderComponent },
  { path: 'btn-guide', component: BtnGuideComponent },
  { path: 'preloader', component: PreloaderComponent },
  { path: 'dropdown', component: DropdownComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'btn-action', component: BtnActionComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
