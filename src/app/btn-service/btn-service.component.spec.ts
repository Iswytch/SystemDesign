import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnServiceComponent } from './btn-service.component';

describe('BtnServiceComponent', () => {
  let component: BtnServiceComponent;
  let fixture: ComponentFixture<BtnServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
