import { Component, OnInit } from '@angular/core';
import { animate, transition, trigger, style, state } from '@angular/animations';


@Component({
  selector: 'app-btn-service',
  templateUrl: './btn-service.component.html',
  styleUrls: ['./btn-service.component.css'],
  animations: [
    trigger('btn-service', [
      state('enabled', style({
        'background-color': '#707070'
      })),
      state('disabled', style({
        'background-color': '#474747'
      })),
    ]),
  ]
})
export class BtnServiceComponent implements OnInit {

  public activateBell = false;
  public activateInfo = false;
  public activateExit = false;
  public activateUser = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimationBell(): void {
    this.activateBell=!this.activateBell;
  }
  runAnimationInfo(): void {
    this.activateInfo=!this.activateInfo;
  }
  runAnimationExit(): void {
    this.activateExit=!this.activateExit;
  }
  runAnimationUser(): void {
    this.activateUser=!this.activateUser;
  }

}
