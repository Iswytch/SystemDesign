import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-voyant',
  templateUrl: './info-voyant.component.html',
  styleUrls: ['./info-voyant.component.css']

})
export class InfoVoyantComponent implements OnInit{

  public text : string = '';

  constructor() { }

  ngOnInit(): void {
  }
}
