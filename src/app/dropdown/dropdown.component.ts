import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css'],
  animations: [
    trigger('fiche-synthese', [
      state('enabled', style({
        height: '10vw',
      })),
      state('disabled', style({
        height: '0vw'
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.09,.81,.61,1)')
      ]),
    ]),
    trigger('img', [
      state('enabled', style({
        transform: 'rotate(180deg)',
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.09,.81,.61,1)')
      ]),
    ]),
    trigger('border', [
      state('enabled', style({
        border: '3px solid grey',
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.09,.81,.61,1)')
      ]),
    ]),
  ]
})
export class DropdownComponent implements OnInit {

  public activate = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimation(): void {
    this.activate = !this.activate;
  }

}
