import { Component, OnInit } from '@angular/core';
import { CdkDragEnd, CdkDragMove } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-drag-and-drop',
  templateUrl: './drag-and-drop.component.html',
  styleUrls: ['./drag-and-drop.component.css']
})
export class DragAndDropComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  /** Evenement quand bouge le bouton. */
  onDragMoved(event: CdkDragMove) {
    document.getElementById('forCdkDragMove')!.style.opacity = '0.5';
  }

  onDragEnded(event: CdkDragEnd) {
    document.getElementById('forCdkDragMove')!.style.opacity = '1';
  }

}
