import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchLargeComponent } from './switch-large.component';

describe('SwitchLargeComponent', () => {
  let component: SwitchLargeComponent;
  let fixture: ComponentFixture<SwitchLargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchLargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
