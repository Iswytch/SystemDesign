import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.css'],
  animations: [
    trigger('filter-panel', [
      state('enabled', style({
        transform: 'translate(-1.5vw)',
      })),
      state('disabled', style({
        transform: 'translate(-18vw)'
      })),
      transition('disabled <=> enabled', [
        animate('1s cubic-bezier(.18,.8,.61,1)')
      ]),
    ]),
    trigger('img', [
      state('enabled', style({
        transform: 'rotate(180deg)',
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.18,.8,.61,1)')
      ]),
    ]),
  ],
})
export class FilterPanelComponent implements OnInit {

  public activateAnimFilter = false;
  public activateAnimImg = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimationFilter(): void {
    this.activateAnimFilter = !this.activateAnimFilter;
  }

  runAnimationImg(): void {
    this.activateAnimImg = !this.activateAnimImg;
  }
}
