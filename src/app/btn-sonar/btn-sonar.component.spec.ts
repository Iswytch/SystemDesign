import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnSonarComponent } from './btn-sonar.component';

describe('BtnSonarComponent', () => {
  let component: BtnSonarComponent;
  let fixture: ComponentFixture<BtnSonarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnSonarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnSonarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
