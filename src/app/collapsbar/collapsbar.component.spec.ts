import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapsbarComponent } from './collapsbar.component';

describe('CollapsbarComponent', () => {
  let component: CollapsbarComponent;
  let fixture: ComponentFixture<CollapsbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollapsbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
