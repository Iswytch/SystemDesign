import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-fiche-synthese',
  templateUrl: './fiche-synthese.component.html',
  styleUrls: ['./fiche-synthese.component.css'],
  animations: [
    trigger('fiche-synthese', [
      state('enabled', style({
        transform: 'translate(1.5vw)',
      })),
      state('disabled', style({
        transform: 'translate(18vw)',
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.18,.8,.61,1)')
      ]),
    ]),
    trigger('img', [
      state('enabled', style({
        transform: 'rotate(180deg)',
      })),
      state('disabled', style({
      })),
      transition('enabled <=> disabled', [
        animate('1s cubic-bezier(.18,.8,.61,1)')
      ]),
    ]),
  ],
})
export class FicheSyntheseComponent implements OnInit {

  public activateAnimFilter = false;
  public activateAnimImg = false;

  constructor() { }

  ngOnInit(): void {
  }

  runAnimationFilter(): void {
    this.activateAnimFilter = !this.activateAnimFilter;
  }

  runAnimationImg(): void {
    this.activateAnimImg = !this.activateAnimImg;
  }
}
