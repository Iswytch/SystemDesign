import { Component, OnInit } from '@angular/core';
import { animate, keyframes, transition, trigger, style } from '@angular/animations';

@Component({
  selector: 'app-btn-trash',
  templateUrl: './btn-trash.component.html',
  styleUrls: ['./btn-trash.component.css'],
  animations: [
    trigger('btn-trash-animation', [
      transition('* => animate', [
        animate('0.3s', keyframes([
          style({'border': '3px solid #707070'}),
          style({'border': '3px solid #B7B7B7'}),
          style({'border': '3px solid #B7B7B7'}),
          style({'border': '3px solid transparant'})
        ]))
      ])
    ])
  ]
})
export class BtnTrashComponent implements OnInit {

  public isClicked = false;
  public linkImgTrash = "assets/images/trash-restore-clair.svg";

  constructor() { }

  ngOnInit(): void {
  }

  // Permet de pouvoir recliquer sur le bouton.
  // La durée du timeout doit être supérieur ou égal à la durée de l'animation.
  runAnimation(): void {
    this.isClicked=true;
    setTimeout(() => {
      this.isClicked=false;
    }, 300);
  }

}
