# SystemDesign

Angular library of graphical components  
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


-----------------


# Configuration NPM


## Version ligne de commande pour Windows

Les certificats du MAA sont disponibles à cette adresse : https://orion.agriculture/artifactory/pro-binaries/certificats/agriculture-ac-serveurs-standard-2010-bundle.crt 

```bash

npm config set registry https://orion.agriculture/artifactory/api/npm/npm-map/ 

npm login -r https://orion.agriculture/artifactory/api/npm/npm-map/ 

npm config set proxy null 

npm config set https-proxy null 

npm config set audit false 

npm config set cafile C:\Outils\bundle.crt
    
```

## Version avec contexte

## Etape 1 : Configuration du repository

Il faut créer un fichier .npmrc dans le home de l'utilisateur :

```bash
registry=https://orion.agriculture/artifactory/api/npm/npm-map/
cafile=/chemin_vers_mon_bundle/chain_bundle.pem
_auth=******
```

NB : attention le fichier .npmrc du home est pris en priorité sur celui du projet.

Il faut ensuite gérer l'authentification sur Artifactory. Le fichier .npmrc doit contenir le login:password de l'utilisateur encodé en BASE64.

Pour obtenir l'encodage du login mot passe, il suffit d'utiliser la commande : 

```bash
$ echo -nE LOGIN:PASSWD | base64
```

LOGIN et PASSWD doivent être remplacer par le login et le mot de passe du développeur. Par exemple :

```bash
$ echo -nE jacquelin.delacrose:Password123456 | base64
```

Attention cependant, le registry https://orion.agriculture/artifactory/api/npm/npm-map/ n'est accessible qu'en lecture seule ! 
Pour pouvoir faire une publication, il faut choisir un registry qui permet de déposer des packages.

Il n'y a pour le moment qu'un seul repo qui permet de déposer des packages, mais est limité en terme d'habilitation :

https://orion.national.agri/artifactory/api/npm/alim-npm-prod-local/


Pour ensuite consulter le repo de notre librairie il faut suivre ce lien :

https://orion.national.agri/artifactory/webapp/#/artifacts/browse/tree/General/alim-npm-prod-local



-----------------


# CREATION ET PUBLICATION DE LA LIBRAIRIE

## Etape 1 : Créer un projet Angular (Partie Application dans le angular.json)


```bash
$ ng new mon-projet
```

## Etape 2 : Créer une librairie  (Partie Library dans le angular.json)


```bash
$ ng generate library mon-projet-lib
```

## Etape 3 : Génerer les composants de notre librarie 


```bash
$ ng generate component foo --project=mon-projet-lib
```

## Etape 4 : Import/Export

Dans mon-projet-lib.module.ts :

```typescript
import { MonProjetLibComponent } from './mon-projet-lib.component';
import { FooComponent } from './foo/foo.component';

@NgModule({
  imports: [
  ],
  declarations: [
    ExampleNg6LibComponent,
    FooComponent
  ],
  exports: [
    ExampleNg6LibComponent,
    FooComponent
  ]
})
```

Dans projects\mon-projet-lib\src\public_api.ts

Ajouter la ligne suivant :

```typescript
export * from './lib/foo/foo.component';
```

## Etape 5 : Build

```bash
$ ng build mon-projet-lib --configuration production
```

## Etape 6 : Publication sur un artifactory

Il faut se rendre dans le fichier dist :

```bash
$ cd .\dist\mon-projet-lib
```

Puis 

```bash
# Si la configuration du .npmrc n'a pas été fait : 
$ npm publish --registry https://orion.national.agri/artifactory/api/npm/alim-npm-prod-local/
# sinon, si le regfistry est déjà déclaré dans le fichier .npmrc :
$ npm publish
```

-----------------


# UTILISATION MANUEL DE LA LIBRAIRIE

## Etape 1

```bash
$ npm i mon-projet-lib
```

## Etape 2

Dans le app.module.ts

```typescript
import { MonProjetLibModule } from 'mon-projet-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MonProjetLibModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Etape 3

Utiliser un composant

```html
<lib-foo></lib-foo>
```

SI NE FONCTIONNE PAS RETOURNER SUR app.module.ts et faire CTRL+S 


## Etape 4

Dans angular.json

```json
"assets": [
  {
    "glob": "**/*",
    "input": "./node_modules/mon-projet-lib/assets/images",
    "output": "./assets/images"
  }
],
```

-----------------


# UTILISATION D'ANGULAR CLI depuis la version 9

## Etape 1

```bash
$ ng add mon-projet-lib
```

## Etape 2

Utiliser un composant

```html
<lib-foo></lib-foo>
```

